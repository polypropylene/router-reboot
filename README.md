## Reboot Script for NETGEAR Orbi Routers

Tested on RBK40 but should also work on similar models such as RBK30, RBK50, RBK 53 etc.

Pre-requisites:\
Python 3.7

Usage: `python reboot.py 192.168.1.1 admin password`\
Replace host and credentials appropriately
